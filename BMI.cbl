       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. Puchong.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT      PIC 999V99  VALUE ZEROES.
       01  HEIGHT      PIC 999     VALUE ZEROES.
       01  HEIGHTM     PIC 99v99     VALUE ZEROES.
       01  BMI         PIC 99V9    VALUE ZEROES.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter your weight(kg.) : " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Enter your height(cm.) : " WITH NO ADVANCING 
           ACCEPT HEIGHT 

           COMPUTE HEIGHTM = (HEIGHT/100) * (HEIGHT/100)
           COMPUTE BMI = WEIGHT / HEIGHTM 
           
           IF BMI < 18.5 THEN
              PERFORM HEADER 
              DISPLAY "Weight : "  WEIGHT " kg."
              DISPLAY "Height : "  HEIGHT " cm."
              DISPLAY "BMI : "  BMI 
              DISPLAY "Body statu : Underweight"
              PERFORM HEADER
           END-IF 

           IF BMI >= 18.5 AND  <= 24.9 THEN
              PERFORM HEADER
              DISPLAY "Weight : "  WEIGHT " kg."
              DISPLAY "Height : "  HEIGHT " cm."
              DISPLAY "BMI : "  BMI 
              DISPLAY "Body statu : Normal"
              PERFORM HEADER
           END-IF 

           IF BMI >= 25 AND <= 29.9 THEN
              PERFORM HEADER
              DISPLAY "Weight : "  WEIGHT " kg."
              DISPLAY "Height : "  HEIGHT " cm."
              DISPLAY "BMI : "  BMI 
              DISPLAY "Body statu : Overweight"
              PERFORM HEADER
           END-IF 

           IF BMI >= 30 AND <= 34.9 THEN
              PERFORM HEADER
              DISPLAY "Weight : "  WEIGHT " kg."
              DISPLAY "Height : "  HEIGHT " cm."
              DISPLAY "BMI : "  BMI 
              DISPLAY "Body statu : Obese"
              PERFORM HEADER
           END-IF 

           IF BMI > 35 THEN
              PERFORM HEADER
              DISPLAY "Weight : "  WEIGHT " kg."
              DISPLAY "Height : "  HEIGHT " cm."
              DISPLAY "BMI : "  BMI 
              DISPLAY "Body statu : Extremly obese"
              PERFORM HEADER
           END-IF 
           GOBACK 
           .
       HEADER.
           DISPLAY "-------------------------------"
           .
       